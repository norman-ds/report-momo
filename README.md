
<!-- README.md is generated from README.Rmd. Please edit that file  -->

# MOMO Report

Das Mortalitäsmonitoring MOMO wurde ursprünglich von der WHO lanciert
und umfasst 24 europäische Ländern deren offizielle nationale
Mortalitätsstatistiken im
[EuroMOMO-Kollaborationsnetzwerks](https://www.euromomo.eu) und die
experimental Statistik
[MOMO](https://www.experimental.bfs.admin.ch/expstat/de/home/innovative-methoden/momo.html)
vom Bundesamt für Statistik *BFS* bereitgestellt werden. Die wöchentlich
erfassten Todesfallzahlen zeigt EuroMOMO über mehrere Jahre, was das BFS
nicht macht. Trotzdem verlangt das Öffentlichkeitsprinzip der Verwaltung
die anonymisierten Daten zu publizieren. In einem Projekt zur
Veranschaulichung von **CI/CD** mit **Microservices** werden die Daten
wöchentlich aufbereitet und als [Mortalitätsmonitoring
Schweiz](https://norman-ds.github.io/momo/) im Web zur Verfügung
gestellt. Der Mehrwert zeigt sich eindeutig.

<!-- badges: start -->

<!-- badges: end -->

Zwei Ziele werden mit diesem Report verfolgt:

1.  Wöchentlich automatisiertes Reporting mit R Markdown.
2.  Transparente Datenquellen über Restschnittstelle laden.

Falls Sie Anregungen oder Fragen zum **MOMO-Report** haben, fühlen Sie
sich bitte frei, diese unter
<https://gitlab.com/norman-ds/report-momo/-/issues> einzureichen.

## API

## Daten

## DevOps

POMO verfolgt einen kontinuierlichen Prozessansatz, der
Softwareentwicklung und IT-Betrieb abdeckt. Alle Scripts sind in
[R](https://www.r-project.org) geschrieben. Wobei für die
reproduzierbare Softwareentwicklung das Kontainersystem von
[Docker](https://hub.docker.com/r/rocker/verse) eingesetzt wird. Als
Versionierungssystem dient [GitLab](https://docs.gitlab.com/ee/ci/),
GitLab *Jobs* für CI/CD und GitLab *Page* als
[Webserver](https://norman-ds.gitlab.io/pomo/).

POMO follows a continuous process approach, covering software
development and IT operations. All scripts are written in R. The Docker
container system is used for reproducible software development. The
versioning system is GitLab, GitLab Jobs for CI/CD and GitLab Page as
web server. Das IDE von RStudio wird in einem Docker gestartet und über
den Browser darauf zugegriffen
(<http://localhost:8787>).

``` yaml
docker run --name REPMOMO -d -p 8787:8787 -v $(pwd):/home/rstudio -e PASSWORD=pwd rocker/verse:3.6.3
```

Das Docker Image von *rocker/verse* hat die R Version 3.6.3 und einige
Packages installiert und erspart uns somit ein Nachinstallieren.

The docker image of *rocker/verse* has the R version 3.6.3 and some
packages installed and saves us from having to install them again.

``` r
sessionInfo()
#> R version 3.6.3 (2020-02-29)
#> Platform: x86_64-pc-linux-gnu (64-bit)
#> Running under: Debian GNU/Linux 10 (buster)
#> 
#> Matrix products: default
#> BLAS/LAPACK: /usr/lib/x86_64-linux-gnu/libopenblasp-r0.3.5.so
#> 
#> locale:
#>  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
#>  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
#>  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=C             
#>  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
#>  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
#> [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
#> 
#> attached base packages:
#> [1] stats     graphics  grDevices utils     datasets  methods   base     
#> 
#> loaded via a namespace (and not attached):
#>  [1] compiler_3.6.3  magrittr_1.5    tools_3.6.3     htmltools_0.4.0
#>  [5] yaml_2.2.1      Rcpp_1.0.4.6    stringi_1.4.6   rmarkdown_2.1  
#>  [9] knitr_1.28      stringr_1.4.0   xfun_0.13       digest_0.6.25  
#> [13] rlang_0.4.5     evaluate_0.14
```

Folgende eine Liste der wenigen verwendeten Packages (mit Version).

The following is a list of the few packages used (with
version).

``` r
libs <- c("curl","jsonlite","readr","dplyr","purrr","ggplot2","lubridate")
ip <- installed.packages(fields = c("Package", "Version"))
ip <- ip[ip[,c("Package")] %in% libs,]
paste(ip[,c("Package")],ip[,c("Version")])
#> [1] "curl 4.3"        "dplyr 0.8.5"     "ggplot2 3.3.0"   "jsonlite 1.6.1" 
#> [5] "lubridate 1.7.8" "purrr 0.3.4"     "readr 1.3.1"
```
